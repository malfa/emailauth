import { createDrawerNavigator } from 'react-navigation-drawer';

// screens
import LoginScreen from '../screens/LoginScreen';
import CustomDrawerContent from '../components/CustomDrawerContent';

const DrawerStack = createDrawerNavigator(
  {
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        gestureEnabled: false,
        drawerLabel: 'Login',
        title: 'Login, sweet Login!' // <=== add this
      }
    }
  },
  
  {
    contentComponent: CustomDrawerContent,
    headerMode: 'none',
    hideStatusBar: false
  }
);

export default DrawerStack;
