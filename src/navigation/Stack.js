import { Icon } from 'native-base';
import React, { Component } from 'react';
import { Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// navigation
import DrawerStack from './DrawerStack';

// screens
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import HomeScreen from '../screens/HomeScreen';
import CardsScreen from '../screens/CardsScreen';
import AddItemScreen from '../screens/AddItemScreen';
import ListScreen from '../screens/ListScreen';

const StackNavigator = createStackNavigator(
  {
    DrawerStack:{
      screen: DrawerStack,
      navigationOptions: {
        gestureEnabled: false,
        drawerLabel: 'Login',
        title: 'Login' // <=== add this
      }
    },

    // Modals
    // /////////////////////////////////////////////////////////////////////////
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        gestureEnabled: false,
        drawerLabel: 'Login',
        title: 'Login' // <=== add this
      }
    },
    RegisterScreen: {
      screen: RegisterScreen,
      navigationOptions: {
        gestureEnabled: false,
        drawerLabel: 'Register',
        title: 'Register' // <=== add this
        
      }
    },
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        gestureEnabled: false,
        drawerLabel: 'Home',
        headerLeft: () => (
          <Icon style={{margin:5, marginLeft:15}}name='menu' />
        ),
        title: 'Поръчки' // <=== add this
      }
     
    },
    CardsScreen: {
      screen: CardsScreen,
      navigationOptions: {
        gestureEnabled: false,
        drawerLabel: 'Cards',
        title: 'Cards' // <=== add this
      }
    },
    AddItemScreen: {
      screen: AddItemScreen,
      navigationOptions: {
        gestureEnabled: false
      }
    },
      ListScreen: {
      screen: ListScreen,
      navigationOptions: {
        gestureEnabled: false
      }
    }
  },
  {
    initialRouteName: 'DrawerStack',
    headerLayoutPreset: 'center',
    mode: 'modal'
  }
);

const App = createAppContainer(StackNavigator);

export default App;
