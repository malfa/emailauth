import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  TextInput,
  Alert
} from 'react-native';

import { db } from '../../config';

let addItem = (title, content, date, group) => {
  db.ref('/anketi').push({
    title: title,
    content: content,
    date: date,
    group: group,
  });
};

export default class AddItem extends Component {

  state = {
    title: '',
    content: '',
    date: '',
    group: ''
  };

  handleChange = e => {
    this.setState({
      title: e.nativeEvent.text,
    });
  };
  handleChange2 = e => {
    this.setState({
      content: e.nativeEvent.text,
    });
  };
  handleChange3 = e => {
    this.setState({
      date: e.nativeEvent.text,
    });
  };
  handleChange4 = e => {
    this.setState({
      group: e.nativeEvent.text,
    });
  };
  handleSubmit = () => {
    addItem(this.state.title, this.state.content, this.state.date, this.state.group);
    Alert.alert('Item saved successfully');
  };

  render() {
    return (
      <View style={styles.main}>
        <Text style={styles.title}>Add Item</Text>
        <TextInput style={styles.itemInput} onChange={this.handleChange} />
        <TextInput style={styles.itemInput} onChange={this.handleChange2} />
        <TextInput style={styles.itemInput} onChange={this.handleChange3} />
        <TextInput style={styles.itemInput} onChange={this.handleChange4} />
        <TouchableHighlight
          style={styles.button}
          underlayColor="white"
          onPress={this.handleSubmit}
        >
          <Text style={styles.buttonText}>Add</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#6565fc'
  },
  title: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'center'
  },
  itemInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'white'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});