import React from 'react';
import { StyleSheet, Text, View, Alert, Keyboard, TouchableWithoutFeedback, Image } from 'react-native';
import * as firebase from 'firebase';
import { Container, Content, Header, Item, Form, Input, Button, Label, Icon } from 'native-base';
import * as Facebook from 'expo-facebook';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class WelcomeScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = ({
            email: '',
            password: '',
            test:[]
        })
    }
    
    componentDidMount(){
        firebase.auth().onAuthStateChanged((user) => {
         console.log(user.providerData[0])
         this.setState({
            test: user.providerData[0]
        })
            if(user != null){
                // console.log(user);
            }
        })
    }
    singUpUser = (email, password) => {
        try {

            if (this.state.password.length < 6) {
                alert("Моля създайте парола с минимум 6 символа!")
                return;
            }

            firebase.auth().createUserWithEmailAndPassword(email, password)


        }
        catch (error) {
            console.log(error.toString())
        }
    }

    loginUser = (email, password) => {
        const mainObject = this.state.test;
        
        console.log('TEST: ' + mainObject)
        try {

            firebase.auth().signInWithEmailAndPassword(email, password).then(function (user) {
                console.log(user.user)
            })

        }

        catch (error) {
            console.log(error.toString())
        }
        this.props.navigation.navigate('HomeScreen', { userEmail: email, userPassword: password, testObject: this.state.test });
    }

    async logIn() {
        try {
            await Facebook.initializeAsync('623922431789741');
            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Facebook.logInWithReadPermissionsAsync({
                permissions: ['public_profile'],
            });
            if (type === 'success') {
                // Get the user's name using Facebook's Graph API
                const response = firebase.auth.FacebookAuthProvider.credential(token)
                firebase.auth().signInWithCredential(response).catch((error) => {
                    console.log(error)
                })
                // console.log(response)

            } else {
                // type === 'cancel'
            }
        } catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }

    render() {
        
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <Container style={styles.container}>

                    <View style={styles.formViewContainer}>
                        <View style={{ width: '100%', justifyContent: 'center', flexDirection: 'row', }}>
                            <Image
                                style={{ width: 100, height: 100, }}
                                source={require('../assets/images/make-money-logo.png')}
                            />
                        </View>
                        <Form>

                            <Item floatingLabel style={{ marginLeft: 10, marginRight: 10 }}>
                                <Label>Email</Label>
                                <Input
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={email => this.setState({ email })}
                                    value={this.state.email}
                                />
                            </Item>

                            <Item floatingLabel style={{ marginLeft: 10, marginRight: 10 }}>
                                <Label>Password</Label>
                                <Input
                                    secureTextEntry={true}
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={password => this.setState({ password })}
                                    value={this.state.password}

                                />
                            </Item>

                        </Form>
                        <Button
                            style={{ marginTop: 30, backgroundColor: 'orange' }}
                            full
                            rounded
                            primary
                            onPress={() => this.loginUser(this.state.email, this.state.password)}
                        >
                            <Text style={{ color: 'white' }}> Login </Text>

                        </Button>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                            <TouchableOpacity>
                                <Text style={{ fontSize: 12, textDecorationLine: 'underline' }}>
                                    Forgoten password?
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('RegisterScreen') }}>
                                <Text style={{ fontSize: 12 }}>
                                    New user? <Text style={{ fontWeight: 'bold' }}>SIGN UP</Text>
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.footer}>

                        <Text style={{ textAlign: 'center', marginTop: 50 }}> Or login with </Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', margin: 10 }}>
                            <Button
                                style={{ marginTop: 10, marginRight: 10 }}
                                full
                                rounded
                                primary
                                onPress={() => this.logIn()}
                            >
                                <Icon type='FontAwesome' name='facebook-official' />
                            </Button>

                            <Button
                                style={{ marginTop: 10, marginLeft: 10 }}
                                full
                                rounded
                                danger
                                onPress={() => this.logIn()}
                            >
                                <Icon type='FontAwesome' name='google' fontSize={20} />
                            </Button>
                        </View>
                    </View>
                </Container>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        padding: 10,
    },
    formViewContainer:{
        flex:2,
        justifyContent:'flex-end'
    },
    footer: {
        width: '100%',
        justifyContent: 'flex-end',
        flex: 1
    }
});