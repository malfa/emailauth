import { Button } from 'native-base';
import React, { Component } from 'react';
import { Icon } from 'native-base';
import { StyleSheet, View, Text, ScrollView, TouchableOpacity,Alert, ActivityIndicator, ImageBackground } from 'react-native';

export default class Home extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      userEmail: this.props.navigation.state.params.userEmail,
      testObject: this.props.navigation.state.params.testObject,
      loading: false,
      ordersObj: [],
      orderBackground: '',
      data: []
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Поръчки',
      headerLeft: (
        <TouchableOpacity  onPress={() =>
          // Works on both iOS and Android
          Alert.alert(
              'Изтриване на поръчка',
              'Сигурни ли сте че искате да изтриете поръчка номер 2172?',
              [

                  {
                      text: 'НЕ',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                  },
                  { text: 'ДА', onPress: () => console.log('OK Pressed') },
              ],
              { cancelable: false },
          )}>
        <Icon name="menu" size={30} style={{ marginStart: 10 }} backgroundColor="#000000"> </Icon>
        </TouchableOpacity>
      ),
    };
  };
  // MOUNT REQUEST
  componentDidMount() {
    this.makeRemoteRequest();
  }

  // SEARCH BAR DATA REQUEST
  makeRemoteRequest = () => {
    const url = `https://api.npoint.io/eb290159c2d9962f1ec0`;
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res.products,
          orderBackground: res.photo_url,
          loading: false,
        });
        // console.log('data ', this.state.data)
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  render() {

    const data = this.state.data
    const ordersBackground = this.state.orderBackground
    console.log('data: ' + data)
    return (
      // <View style={{ flex: 1 }}>
      //   <Text style={{ fontSize: 20, padding: 5, textAlign: 'center' }}>Hello, <Text style={{ fontWeight: 'bold' }}>{this.state.testObject.displayName}</Text>! Nice to see you again!</Text>
      //   <Button
      //     title="Add an Item"
      //     onPress={() => this.props.navigation.navigate('AddItemScreen')}
      //   />
      //   <Button
      //     title="List of Items"
      //     color="green"
      //     onPress={() => this.props.navigation.navigate('ListScreen')}
      //   />
      // </View>
      <ScrollView>

        {
          data.map((u, i) => {
            const test = u.order;
            console.log('test ' + test)
            return (
              <TouchableOpacity style={{ padding: 10 }} key={i} onPress={() => {
                this.props.navigation.navigate('Home')
              }}>
                <ImageBackground style={{ width: '100%', height: 100 }} source={{ uri: ordersBackground }}>
                  <View style={{ backgroundColor: 'rgba(45,45,55,0.5)', width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ padding: 10, textAlign: 'center', color: '#fff' }} >Доставка до: {u.address}</Text>

                  </View>
                </ImageBackground>
                {
                  test.map((o, i) => {

                    return (
                      <View key={i} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '10%', justifyContent: 'center', alignItems: 'center' }}>
                          <Text style={styles.ordersNumberStyle}>{o.orderCount}</Text>
                        </View>
                        <View style={{ width: '70%', justifyContent: 'center', alignItems: 'center' }}>
                          <Text style={styles.ordersNameStyle}>{o.orderName}</Text>

                        </View>
                        <View style={{ width: '20%', justifyContent: 'center', alignItems: 'center' }}>
                          <Text style={styles.ordersPriceStyle}>{(Math.round(o.orderPrice * 100) / 100).toFixed(2)} лв.</Text>

                        </View>
                      </View>
                    )
                  })
                }

                <View style={{ marginTop: 15, flexDirection: 'row', justifyContent: 'space-around' }}>
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ marginLeft: 15, fontWeight: 'bold', }}>Общо: {(Math.round(u.orderTotalPrice * 100) / 100).toFixed(2)} лв.</Text>
                  </View>
                  {
                    u.status == 1 ?
                      <View style={{ flexDirection: 'row' }}>
                        <Button success style={{ marginRight: 10 }}>
                          <Text style={{ color: '#fff', paddingLeft: 15, paddingRight: 15 }}>Приеми</Text>
                        </Button>
                        <Button success transparent bordered>
                          <Text style={{ color: 'green', paddingLeft: 15, paddingRight: 15 }}>Откажи</Text>
                        </Button>
                      </View> : null
                  }
                  {
                    u.status == 2 ?
                      <View style={{ flexDirection: 'row' }}>
                        <Button success transparent bordered>
                          <Text style={{ color: 'green', paddingLeft: 15, paddingRight: 15 }}>Поръчката е приета</Text>
                        </Button>
                      </View> : null
                  }
                  {
                    u.status == 3 ?
                      <View style={{ flexDirection: 'row' }}>
                        <Button success transparent bordered>
                          <Text style={{ color: 'green', paddingLeft: 15, paddingRight: 15 }}>Поръчката е на път</Text>
                        </Button>
                      </View> : null
                  }
                  {
                    u.status == 4 ?
                      <View style={{ flexDirection: 'row' }}>
                        <Button success transparent bordered>
                          <Text style={{ color: 'green', paddingLeft: 15, paddingRight: 15 }}>Поръчката е завършена</Text>
                        </Button>
                      </View> : null
                  }

                </View>

                <View style={{ height: 2, width: '100%', backgroundColor: '#d3d3d3', marginTop: 10 }}></View>

              </TouchableOpacity>
            )
          })
        }
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  ordersNumberStyle: {
    textAlign: 'center',
    paddingLeft: 9,
    fontSize: 18,
    fontWeight: 'bold',
    paddingRight: 9,
    borderWidth: 2,
    borderRadius: 4,
    borderColor: 'green',
    color: 'green',
    marginTop: 5
  },
  ordersNameStyle: {
    paddingLeft: 10,
    borderWidth: 2,
    borderRadius: 4,
    width: '100%',
    borderColor: 'transparent',
    marginTop: 5
  },
  ordersPriceStyle: {
    paddingLeft: 10,
    borderWidth: 2,
    borderRadius: 4,
    borderColor: 'transparent',
    marginTop: 5,
  }
});