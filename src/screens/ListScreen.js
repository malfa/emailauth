import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, TextInput } from 'react-native';
import ItemComponent from '../components/ItemComponent';

import { db } from '../../config';
let itemsRef = db.ref('/anketi');

export default class ListScreen extends Component {
  state = {
    items: []
  };
  

  deleteData() {
    itemsRef.remove()
    itemsRef.child('mike').update({'dateOfBirth': moment(value.dateOfBirth).toDate().getTime()})
    this.forceUpdate()
  }
  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);

      this.setState({ items });


    });
  }

  handleChange = e => {
    this.setState({
      title: e.nativeEvent.text,
    });
  };
  handleChange2 = e => {
    this.setState({
      content: e.nativeEvent.text,
    });
  };
  handleChange3 = e => {
    this.setState({
      date: e.nativeEvent.text,
    });
  };
  handleChange4 = e => {
    this.setState({
      group: e.nativeEvent.text,
    });
  };
  handleUpdate = () => {
    addItem(this.state.title, this.state.content, this.state.date, this.state.group);
    Alert.alert('Item update successfully');
  };

  render() {
    console.log('tova li mi vru6ta: ' + this.state.items);
    return (
      <View style={styles.container}>
        {this.state.items.length > 0 ? (
          <ItemComponent items={this.state.items} />
        ) : (
            <Text>No items</Text>
          )}
          <TextInput style={styles.itemInput} onChange={this.handleChange} />
        <TextInput style={styles.itemInput} onChange={this.handleChange2} />
        <TextInput style={styles.itemInput} onChange={this.handleChange3} />
        <TextInput style={styles.itemInput} onChange={this.handleChange4} />
        <Button title="Update data" onPress={this.handleSubmit}></Button>
        <Button color='red' title="Delete data" onPress={() => this.deleteData()}></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ebebeb'
  }
});
