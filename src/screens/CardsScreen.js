import React from 'react';
import {
  ActivityIndicator,
  Button,
  Dimensions,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

import * as firebase from 'firebase';
import firebaseConfig from '../../config';


if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export default class CardsScreen extends React.Component {

  constructor(props) {
    super(props)

    this.state = ({
      title: '',
      discription: '',
      fullname: '',
      items: []
    })
  }


  readUserData() {
    firebase.database().ref('UsersList/').once('value', function (snapshot) {
        console.log(snapshot.val())
    }).then((snapshot) => {

        console.log(snapshot.val())
        let items = [];

        snapshot.forEach((child) => {
          items.push({
            title: child.val().title,
            fullname: child.val().fullname,
            description: child.val().description,
          });
        })

        this.setState({
          items: snapshot.val()
        });

        console.log('omfg ' + this.state.items);

      });


  }

  render() {
    const test = this.state.items
    console.log('TTTTTTTTTTTTTTTTT ' + test)
    return (
      <View style={styles.container}>
        <Text style={styles.title}>
          Store some value in Firebase!
        </Text>

        <Button
          onPress={() => this.readUserData()}
          title="Read"
        />
        <View><Text></Text></View>
      </View>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
  },
  textInput: {
    width: Dimensions.get('window').width - 30,
    marginHorizontal: 15,
    padding: 5,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '#eee',
    marginVertical: 15,
    height: 50,
    fontSize: 16,
  }
});
