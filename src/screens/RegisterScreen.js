import React from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { Container, Form, Item, Label, Button, Input} from 'native-base';
import * as firebase from 'firebase';

export default class RegisterScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = ({
            email: '',
            password: '',
            displayName: '',
            phoneNumber: ''
        })
    }

    singUpUser = (email, password, displayName, phoneNumber, photoURL) => {
        try {

            if (this.state.password.length < 6) {
                alert("Your password needs to be at least 6 characters!")
                return;
            }

            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then((res) => {
                    res.user.updateProfile({
                        displayName: this.state.displayName,
                        phoneNumber: this.state.phoneNumber
                    })
                    Alert.alert(
                        "",
                        "You successfully registered!",
                        [

                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ],
                        { cancelable: false }
                    );
                    this.setState({
                        displayName: '',
                        email: '',
                        password: '',
                        phoneNumber:''
                    })
                    this.props.navigation.navigate('LoginScreen')
                })
        }
        catch (error) {
            console.log(error.toString())
        }
    }



    render() {

        return (
            <Container style={styles.container}>
                <Text style={{ textAlign: 'center' }}>REGISTER</Text>
                <Form>

                    <Item floatingLabel>
                        <Label>User name</Label>
                        <Input
                            autoCorrect={false}
                            autoCapitalize="none"
                            keyboardType={"name-phone-pad"}
                            onChangeText={displayName => this.setState({ displayName })}
                            value={this.state.displayName}
                        />
                    </Item>

                    <Item floatingLabel>
                        <Label>PhoneNumber</Label>
                        <Input
                            autoCorrect={false}
                            keyboardType={"number-pad"}
                            autoCapitalize="none"
                            onChangeText={phoneNumber => this.setState({ phoneNumber })}
                            value={this.state.phoneNumber}
                        />
                    </Item>

                    <Item floatingLabel>
                        <Label>Email</Label>
                        <Input
                            autoCorrect={false}
                            keyboardType={"email-address"}
                            autoCapitalize="none"
                            onChangeText={email => this.setState({ email })}
                            value={this.state.email}
                        />
                    </Item>

                    <Item floatingLabel>
                        <Label>Password</Label>
                        <Input
                            secureTextEntry={true}
                            autoCorrect={false}
                            autoCapitalize="none"
                            onChangeText={password => this.setState({ password })}
                            value={this.state.password}
                        />
                    </Item>


                    <Button
                        style={{ marginTop: 30, backgroundColor: 'orange' }}
                        full
                        rounded
                        primary
                        onPress={() => this.singUpUser(this.state.email, this.state.password)}
                    >
                        <Text style={{ color: 'white' }}> Sing Up </Text>
                    </Button>

                </Form>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        padding: 10
    }
});