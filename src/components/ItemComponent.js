import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { Card } from 'react-native-elements';
import PropTypes from 'prop-types';

export default class ItemComponent extends Component {
    static propTypes = {
        items: PropTypes.array.isRequired
    };


    render() {
        return (
            <View style={styles.itemsList}>
                {this.props.items.map((item, index) => {
                    return (
                        <Card key={index}>
                            <Text style={styles.itemtext}>{item.title}</Text>
                            <Text style={styles.content}>{item.content}</Text>
                            <Text style={styles.itemtext}>{item.date}</Text>
                            <Text style={styles.group}>{item.group}</Text>
                        </Card>
                    );
                })}
               
            </View>
        );
    }
}

const styles = StyleSheet.create({
    itemsList: {
        flex: 1,
        flexDirection: 'column',
    },
    itemtext: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    content:  {
        fontSize: 16,
        textAlign: 'center'
    },
    group: {
        fontSize: 16,
        textAlign: 'center',
        textDecorationLine:'underline'
    }
});