
import React from 'react';
import * as firebase from 'firebase';
import { StatusBar, YellowBox  } from 'react-native';
import { AppLoading } from 'expo';
import { func } from './src/constants';
import _ from 'lodash';

import Stack from './src/navigation/Stack';
import firebaseConfig from './config';

// workaround for firebase warning bug https://github.com/firebase/firebase-js-sdk/issues/97
YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}


export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    };
  }

  render() {
    const { isLoading } = this.state;

    if (isLoading) {
      return (
        <AppLoading
          onFinish={() => this.setState({ isLoading: false })}
          startAsync={func.loadAssetsAsync}
        />
      );
    }

    return (
      <React.Fragment>
        <StatusBar barStyle="dark-content" />
        <Stack />
      </React.Fragment>
    );
  }
}
